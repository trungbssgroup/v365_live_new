<?php
/**
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Magemaven
 * @package     Magemaven_OrderComment
 * @copyright   Copyright (c) 2011-2012 Sergey Storchay <r8@r8.com.ua>
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class Magemaven_OrderComment_Model_Observer extends Varien_Object
{
    /**
     * Save comment from agreement form to order
     *
     * @param $observer
     */
    public function saveOrderComment($observer)
    {
        $orderComment = Mage::app()->getRequest()->getPost('ordercomment');
		 $ptest = Mage::app()->getRequest()->getPost('ptest');
		 $ptest1 = Mage::app()->getRequest()->getPost('ptest1');
		 $ptest2 = Mage::app()->getRequest()->getPost('ptest2');
		 $ptest3 = Mage::app()->getRequest()->getPost('ptest3');
		 $ptest4 = Mage::app()->getRequest()->getPost('ptest4');
		 $ptest5 = Mage::app()->getRequest()->getPost('ptest5');
		 $ptest6 = Mage::app()->getRequest()->getPost('ptest6');
		 $ptest7 = Mage::app()->getRequest()->getPost('ptest7');
        if (is_array($orderComment) && isset($orderComment['comment']) || is_array($ptest) && isset($ptest['comt'])  || is_array($ptest1) && isset($ptest['comt1'])  || is_array($ptest2) && isset($ptest['comt2'])  || is_array($ptest3) && isset($ptest['comt3'])  || is_array($ptest4) && isset($ptest['comt4'])  || is_array($ptest5) && isset($ptest['comt5'])  || is_array($ptest6) && isset($ptest['comt6'])  || is_array($ptest7) && isset($ptest['comt7'])) {
            $comment =  trim($ptest['comt']).'  '.trim($ptest1['comt1']).'  '.trim($ptest2['comt2']).'  '.trim($ptest3['comt3']).'  '.trim($ptest4['comt4']).'  '.trim($ptest5['comt5']).'  '.trim($ptest6['comt6']).'  '.trim($ptest7['comt7']).'  '.trim($orderComment['comment']);

            if (!empty($comment)) {
                $order = $observer->getEvent()->getOrder(); 
                $order->setCustomerComment($comment);
                $order->setCustomerNoteNotify(true);
                $order->setCustomerNote($comment);
            }
        }
    }
}