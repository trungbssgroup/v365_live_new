<?php
/**
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Magemaven
 * @package     Magemaven_OrderComment
 * @copyright   Copyright (c) 2011-2012 Sergey Storchay <r8@r8.com.ua>
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class Magemaven_OrderComment_Model_Observer extends Varien_Object
{
    /**
     * Save comment from agreement form to order
     *
     * @param $observer
     */
    public function saveOrderComment($observer)
    {
        $orderComment = Mage::app()->getRequest()->getPost('ordercomment');
		 $ptest = Mage::app()->getRequest()->getPost('ptest');
		 $ptest1 = Mage::app()->getRequest()->getPost('ptest1');
        if (is_array($orderComment) && isset($orderComment['comment']) || is_array($ptest) && isset($ptest['comt'])  || is_array($ptest1) && isset($ptest['comt1'])) {
            $comment =  trim($ptest['comt']).'  '.trim($ptest1['comt1']).'  '.trim($orderComment['comment']);

            if (!empty($comment)) {
                $order = $observer->getEvent()->getOrder(); 
                $order->setCustomerComment($comment);
                $order->setCustomerNoteNotify(true);
                $order->setCustomerNote($comment);
            }
        }
    }
}