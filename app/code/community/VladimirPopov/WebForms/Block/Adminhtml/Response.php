<?php
class VladimirPopov_WebForms_Block_Adminhtml_Response
	extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_response';
		$this->_blockGroup = 'webforms';
		$this->_headerText = Mage::helper('webforms')->__('Manage Canned Responses');
		parent::__construct();
	}
	
}
?>
