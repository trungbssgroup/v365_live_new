<?php
class VladimirPopov_WebForms_Adminhtml_ResponseController
	extends Mage_Adminhtml_Controller_Action
{
	public function indexAction(){
		$this->loadLayout();
		
		// add grid
		$this->getLayout()->getBlock('content')->append(
			$this->getLayout()->createBlock('webforms/adminhtml_response')
		);
		
		$this->renderLayout();
	}
	
	public function gridAction()
	{
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('webforms/adminhtml_response_grid')->toHtml()
		);
	}	

	public function newAction()
	{
		$this->_forward('edit');
	}
	
	public function editAction()
	{
		$this->loadLayout();
		
		// add grid
		$this->getLayout()->getBlock('content')->append(
			$this->getLayout()->createBlock('webforms/adminhtml_response_edit')
		);
		
		$this->renderLayout();
		
	}
	
	public function saveAction()
	{
		if( $this->getRequest()->getPost()){
			try{
				$postData = $this->getRequest()->getPost('response');
				$id = $postData['response_id'];
				$saveandcontinue = $postData["saveandcontinue"];			
				
				$response = Mage::getModel('webforms/response')->setData($postData);
				
				if($id)$response->setId($id);
				
				$response->save();

				if( $id <= 0 )
					$response->setCreatedTime(Mage::getSingleton('core/date')->gmtDate())->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Response was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				
				if($saveandcontinue){
					$this->_redirect('*/adminhtml_response/edit',array('id' => $response->getId()));
				} else {
					$this->_redirect('*/adminhtml_response/index');
				}
				return;
			} catch (Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($postData);
				$this->_redirect('*/*/edit',array('id' => $this->getRequest()->getParam('id')));
				return;
			}
			
		}
		$this->_redirect('*/adminhtml_response/index');
		
	}
	
	public function deleteAction()
	{
		if( $this->getRequest()->getParam('id') > 0){
			try{
				$response = Mage::getModel('webforms/response');
				$response->load($this->getRequest()->getParam('id'));
				$response->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Response was successfully deleted'));
				$this->_redirect('*/adminhtml_response/index');
			} catch (Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/adminhtml_response/index');		
	}
	
	public function massDeleteAction()
	{
		$Ids = (array)$this->getRequest()->getParam('id');
		
		try {
			foreach($Ids as $id){
				$response = Mage::getModel('webforms/response')->load($id);
				$response->delete();
			}

			$this->_getSession()->addSuccess(
				$this->__('Total of %d record(s) have been deleted.', count($Ids))
			);
		}
		catch (Mage_Core_Model_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		}
		catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		}
		catch (Exception $e) {
			$this->_getSession()->addException($e, $this->__('An error occurred while updating records.'));
		}

		$this->_redirect('webforms/adminhtml_response/index');
		
	}
	
	public function getAjaxMessageAction()
	{
		$id = $this->getRequest()->getPost('id');
		
		if($id){
			$response = Mage::getModel('webforms/response')->load($id);
			$this->getResponse()->setBody($response->getMessage());
		}
	}

}
?>
