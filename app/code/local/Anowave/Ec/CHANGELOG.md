# Changelog

All notable changes to this project will be documented in this file.

## [21.0.1]

### Added

- Newsletter subscription tracking (event based)

## [21.0.0]

### New branch

## [20.0.9]

### Added

- Affiliation tracking (based on URI parameter). Affiliation parmater is automatically converted to product scoped dimension and passed with impression, click/add/remove events, transactions etc.

## [20.0.8]

### Added

- Extended API to allow for creating Universal Analytics tag automatically

## [20.0.5]

### Changed

- Code cleanup

### Added

- Added a new API configuration section related to Google Measurement Protocol and offline order tracking

## [20.0.4]

- Fixed Product list attribution from cart additions from categories

## [20.0.0]

- Added fallback to collections with no toolbars

## [19.0.9]

###Fixed

- Fixed ecomm_ price discrepancies / Added Incl. tax on all instances

## [19.0.8]

###Fixed

- Wrong tax calculation for bundle items in AdWords Dynamic Remarketing

## [19.0.7] 

###Changed

- Minor updates
- Cleanup

## [19.0.6] 

###Changed

- Minor updates

## [19.0.5]

### Fixed

- Fixed potential XSS vulnerability in search results

## [19.0.4]

### Fixed

Remove from cart was not working for configurable products (in some versions of Magento)

## [19.0.3]

### Added

"Add to wishlist" tracking from categories