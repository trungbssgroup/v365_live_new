<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Product Labels
 * @version   1.0.4
 * @build     370
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_CatalogLabel_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getProductHtml($placeholderCode, $product, $type, $template, $width = null, $height = null)
    {
        return Mage::getSingleton('core/layout')
                ->createBlock('cataloglabel/product_label')
                ->setType($type)
                ->setTemplate('mst_cataloglabel/product/'.$template.'.phtml')
                ->setPlaceholderCode($placeholderCode)
                ->setProduct($product)
                ->setWidth($width)
                ->setHeight($height)
                ->toHtml();
    }

    public function getProductListHtml($placeholderCode, $product, $width = null, $height = null)
    {
        return $this->getProductHtml($placeholderCode, $product, 'list', 'list', $width, $height);
    }

    public function getProductViewHtml($placeholderCode, $product, $width = null, $height = null)
    {
        return $this->getProductHtml($placeholderCode, $product, 'view', 'view', $width, $height);
    }

    public function getFullActionCode()
    {
        $request = Mage::app()->getRequest();
        return strtolower($request->getModuleName().'_'.$request->getControllerName().'_'.$request->getActionName());
    }
}