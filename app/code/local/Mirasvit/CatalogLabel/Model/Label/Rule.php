<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Product Labels
 * @version   1.0.4
 * @build     370
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_CatalogLabel_Model_Label_Rule extends Mage_Rule_Model_Rule
{
    protected $_productIds;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('cataloglabel/label_rule');
        $this->setIdFieldName('rule_id');
    }

    public function getDisplay()
    {
        $display = Mage::getModel('cataloglabel/label_display');
        if ($this->getDisplayId()) {
            $display->load($this->getDisplayId());
        }

        return $display;
    }

    public function getLabel()
    {
        $label = Mage::getModel('cataloglabel/label');
        if ($this->getLabelId()) {
            $label->load($this->getLabelId());
        }

        return $label;
    }

    public function getConditionsInstance()
    {
        return Mage::getModel('cataloglabel/label_rule_condition_combine');
    }

    public function getActionsInstance()
    {
        return Mage::getModel('cataloglabel/label_rule_action_collection');
    }

    protected function _afterSave()
    {
        $this->_getResource()->updateRuleProductData($this);
        parent::_afterSave();
    }

    public function getMatchingProductIds()
    {
        if (is_null($this->_productIds)) {
            $this->_productIds = array();
            $this->setCollectedAttributes(array());

            $productCollection = Mage::getResourceModel('catalog/product_collection');

            $this->getConditions()->collectValidatedAttributes($productCollection);

            Mage::getSingleton('core/resource_iterator')->walk(
                $productCollection->getSelect(),
                array(array($this, 'callbackValidateProduct')),
                array(
                    'attributes' => $this->getCollectedAttributes(),
                    'product'    => Mage::getModel('catalog/product'),
                )
            );
        }

        return $this->_productIds;
    }

    public function callbackValidateProduct($args)
    {
        $product = clone $args['product'];
        $product->setData($args['row']);

        if ($this->getConditions()->validate($product)) {
            $this->_productIds[] = $product->getId();
        }
    }

    public function getProductIds()
    {
        return $this->_getResource()->getRuleProductIds($this->getId());
    }

    public function isProductId($productId)
    {
        return $this->_getResource()->isRuleProductId($this->getId(), $productId);
    }
}